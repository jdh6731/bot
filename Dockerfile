FROM golang:1.19.0 AS build

WORKDIR /app

ENV GOARCH=386
ENV GOOS=linux
ENV GO111MODULE=on
ENV CGO_ENABLED=0

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /devopsbot

FROM alpine:3.16.2

WORKDIR /app

COPY devopsbot /app

CMD ["/app/devopsbot"]
